import {Subject} from "rxjs";

const messageSubject = new Subject();

export const notificationService = {
  setMessage: (message, type, dismissible) => messageSubject.next({
    text: message,
    type: type,
    dismissible: dismissible
  }),
  clearMessage: () => messageSubject.next(),
  getMessage: () => messageSubject.asObservable(),
}
