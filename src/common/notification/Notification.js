import React from "react";
import {notificationService} from "./NotificationService";

class NotificationComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      message: null,
    }
  }

  componentDidMount() {
    this.messageSubscription = notificationService.getMessage().subscribe(message => {
      this.setState({
        message: message,
      })
    });
  }

  componentWillUnmount() {
    this.messageSubscription.unsubscribe();
  }

  render() {
    let alertClass = "alert alert-";
    if (this.state.message) {
      alertClass += this.state.message.type;
      if (this.state.message.dismissible) {
        alertClass += " alert-dismissible fade show";
      }
    }

    return (
      <div>
        {this.state.message &&
        <div className={alertClass}
             role="alert">
          <span>{this.state.message.text}</span>

          {this.state.message.dismissible &&
          <button type="button" className="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          }
        </div>
        }
      </div>
    );
  }
}

export default NotificationComponent
