
export function formatCurrency(value) {
  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD"
  }).format(value)
}

export function formatDateToTime(value) {
  return new Intl.DateTimeFormat('en-GB', {
    hour: "numeric",
    minute: "2-digit",
    hour12: true,
  }).format(value).toUpperCase()
}