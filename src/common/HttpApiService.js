import {loadingService} from "./loading/LoadingService";
import {notificationService} from "./notification/NotificationService";
import {API_URL} from "./environment";

async function getErrorText(response) {
  const data = await response.json();

  // get error message from body or default to response statusText
  const error = (data && data.message) || response.statusText;
  if (error) {
    notificationService.setMessage(error, "danger", false);
    return error;
  }

  return JSON.stringify(data);
}

export function handleHttpErrors(response) {
  loadingService.setLoading(false);

  return new Promise((resolve, reject) => {

    if (!response.ok) {
      getErrorText(response)
        .then(response => reject(response));
    } else {
      resolve(response);
    }

  });
}

export function onError(error, entityRequested) {
  console.error(error)
  loadingService.setLoading(false);
  notificationService.setMessage("Error when getting " + entityRequested +": " + error.message, "danger", false);
}

export const httpApiService = {

  fetchData(apiUrl) {
    loadingService.setLoading(true);
    notificationService.clearMessage();

    return fetch(API_URL + apiUrl)
      .then(response => handleHttpErrors(response))
      .then(response => response.json());
  }
}

export default httpApiService
