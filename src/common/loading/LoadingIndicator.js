import React from "react";
import {loadingService} from "./LoadingService";

class LoadingIndicatorComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    }
  }

  componentDidMount() {
    this.loadingSubscription = loadingService.isLoading().subscribe(loading => {
      this.setState(() => {
        return {
          loading: loading,
        };
      })
    });
  }

  componentWillUnmount() {
    this.loadingSubscription.unsubscribe();
  }

  render() {
    return (
      <div>
        {this.state.loading &&
        <progress className=":indeterminate" />
        }
      </div>
    );
  }
}

export default LoadingIndicatorComponent
