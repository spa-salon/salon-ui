import {Subject} from "rxjs";

const loadingSubject = new Subject();

export const loadingService = {
  setLoading: (loading) => loadingSubject.next(loading),
  isLoading: () => loadingSubject.asObservable(),
}