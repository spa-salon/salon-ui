import './ChooseSlot.css';
import React from "react";
import httpApiService, {onError} from "../common/HttpApiService";
import {notificationService} from "../common/notification/NotificationService";
import {formatDateToTime} from "../common/Formatting";


class ChooseSlot extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      serviceId: null,
      serviceName: null,
      slotsLoaded: false,
    };
  }

  componentDidMount() {
    const {match: {params}} = this.props;
    this.setState({
      serviceId: params.serviceId,
      serviceName: params.serviceName,
      availableSlots: [],
      slotsLoaded: false,
    });
  }

  onDateChange(requestedDate) {
    notificationService.clearMessage();

    this.setState(prevState => {
      return {
        ...prevState,
        requestedDate: requestedDate,
        slotsLoaded: false,
      }
    })
  }

  searchAvailableSlotsForDate() {
    const requestedDate = new Date(this.state.requestedDate);
    const now = new Date();
    if (requestedDate.getTime() < now.getTime()) {
      notificationService.setMessage("The requested date is before now", "danger", false);
    } else {
      this.getAvailableSlots();
    }
  }

  getAvailableSlots() {
    const availableSlotsForSalonServicesUrl = 'slots/retrieveAvailableSlots/' + this.state.serviceId + '/' + this.state.requestedDate;

    httpApiService.fetchData(availableSlotsForSalonServicesUrl)
      .then(data => this.defineState(data))
      .catch(error => {
        onError(error, "Available Slots")
        this.defineState([]);
      })
  }

  defineState(allSlots) {
    let todayAtMidnight = new Date()
    todayAtMidnight.setHours(0, 0, 0, 0);


    const slots = allSlots.filter(slot => {
      const slotFor = new Date(slot.slotFor);
      return slotFor.getTime() >= todayAtMidnight.getTime();
    }).sort((s1, s2) => new Date(s1.slotFor).getTime() - new Date(s2.slotFor).getTime());

    const slotsAvailable = slots !== null && slots.length > 0;

    this.setState({
      availableSlots: slots || [],
      serviceId: this.state.serviceId,
      serviceName: this.state.serviceName,
      slotsLoaded: true,
      areSlotsAvailable: slotsAvailable,
    });
  }

  bookSlotFor(slot) {
  }

  render() {
    return (
      <div className="ChooseSlot mt-3">

        <form className="ChooseSlot__form mb-3" onSubmit={event => {event.preventDefault(); this.searchAvailableSlotsForDate()}}>
          <div className="form-group row">
            <label className="col-sm-7 col-form-label font-weight-bolder">Choose a date for {this.state.serviceName}:
            </label>
            <div className="col-sm-5">
              <input type="date" id="requestedDate" name="requestedDate"
                     className="form-control"
                     onChange={event => this.onDateChange(event.target.value)}/>
            </div>
          </div>

          <input type="submit"
                 className="btn btn-primary"
                 disabled={!this.state.requestedDate}
                 value="Show Slots"/>
        </form>

        {this.state.slotsLoaded && !this.state.areSlotsAvailable &&
        <h6><em>No slots available for {this.state.serviceName}</em></h6>
        }

        {this.state.slotsLoaded && this.state.areSlotsAvailable &&
        <div className="ChooseSlot__container">
          <h4>Available Slots on {this.state.requestedDate}</h4>

          <div className="ChooseSlot__available-slots__container">
            {this.state.availableSlots.map(slot => {
                return <div className="card mb-3 ChooseSlot__slot" key={slot.id}>
                  <div className="card-header">{this.state.serviceName}</div>
                  <div className="card-body">
                    <p className="card-title"><strong>{slot.stylistName}</strong></p>
                    <p className="card-text">Slot Time &nbsp; {formatDateToTime(new Date(slot.slotFor))}</p>
                  </div>
                  <div>
                    <button type="button" className="btn btn-outline-primary btn-sm m-2"
                            onClick={() => this.bookSlotFor(slot)}>Book this slot
                    </button>
                  </div>
                </div>
              }
            )}
          </div>
        </div>
        }
      </div>
    );
  }
}

export default ChooseSlot
