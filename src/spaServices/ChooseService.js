import './ChooseService.css';
import React from "react";
import httpApiService, {onError} from "../common/HttpApiService";
import {generatePath} from "react-router-dom";
import {formatCurrency} from "../common/Formatting";

class ChooseServiceComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      spaServices: [],
    };
  }

  componentDidMount() {
    const availableSalonServicesUrl = 'services/retrieveAvailableSalonServices';

    httpApiService.fetchData(availableSalonServicesUrl)
      .then(data => this.defineState(data))
      .catch(error => {
        onError(error, "Spa services");
        this.defineState([]);
      });
  }

  defineState(services) {
    this.setState({
      spaServices: services || [],
    });
  }

  chooseSlot(service) {
    let pathToSlot = generatePath("/chooseslot/:serviceId/:serviceName", {
      serviceId: service.id,
      serviceName: service.name,
    });

    this.props.history.push(pathToSlot);
  }

  render() {
    return (
      <div className="ChooseService">

        {this.state.spaServices !== null && this.state.spaServices.length > 0 &&
        <div className="ChooseService_container">
          {this.state.spaServices.map(spaService => {
              return <div className="card mb-3 ChooseService_service" key={spaService.id}>
                <div className="card-header">{spaService.name}</div>
                <div className="card-body">
                  <p className="card-title">
                    <strong>{formatCurrency(spaService.price)}</strong>
                  </p>
                  <p className="card-text m-3">{spaService.description}</p>
                  <p className="card-text">{spaService.timeInMinutes} Minutes</p>
                </div>
                <div>
                  <button type="button" className="btn btn-outline-primary btn-sm m-2"
                          onClick={() => this.chooseSlot(spaService)}>Book now</button>
                </div>
              </div>
            }
          )}
        </div>
        }
      </div>
    );
  }

}

export default ChooseServiceComponent
