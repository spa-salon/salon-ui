import './App.css';
import React from "react";
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import LoadingIndicator from "./common/loading/LoadingIndicator";
import Notification from "./common/notification/Notification";
import ChooseService from "./spaServices/ChooseService";
import ChooseSlot from "./slots/ChooseSlot";


class App extends React.Component {

  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">

            <nav className="navbar navbar-expand-lg navbar-light">
              <Link className="App-header-home navbar-brand " to="/">AR Salon and Day Spa</Link>
            </nav>

          </header>

          <main>
            <LoadingIndicator/>

            <Notification/>

            <Switch>
              <Route exact path="/" component={ChooseService}/>
              <Route path="/chooseslot/:serviceId/:serviceName" component={ChooseSlot}/>
              <Route>
                <ChooseService/>
              </Route>
            </Switch>
          </main>

        </div>
      </Router>
    );
  }
}

export default App;
